
let y = 200;
let i = 500;
let dia = 100;

let angle = 0;

let move_button;
let move_buttonStatus = false;


let reset_button;
let reset_buttonStatus = false;

let img;

let textfield;

function preload()
{
  img = loadImage('images/1_2.jpg')
}

function change_background() {


    r = random(255);

    g = random(255);

    b = random(255);

    background(r, g, b);
}

function move_wheel() {

    background(220);
    fill(255, 0, 255);
    noStroke();
    //circle(i,y,dia);
    ellipse(i, y, dia, dia);

    stroke(100);
    strokeWeight(5);
    line(500, 280, i, 280);

    if(i >= 185.5){
      i = i-1;
    }
    else {

      stroke(color(2, 204, 0));
      strokeWeight(5);

      line(185.5,254,185.5,248);
      move_buttonStatus = false;
    }
}

function button_status() {
  move_buttonStatus = true;
}

function reset_wheel() {
  move_buttonStatus = false;
  y = 200;
  i = 500;
  background(220);
  fill(255, 0, 255);
  noStroke();
  ellipse(i, y, dia, dia);
}

function setup() {
  createCanvas(600, 300);
  background(220);
  textfield = createInput('Enter Dia of Wheel');
  textfield.position(50,25);
  move_button = createButton("Move Wheel");
  move_button.position(500, 25);

  reset_button = createButton("Reset Wheel");
  reset_button.position(400, 25);

  fill(255, 0, 255);
  noStroke();
  ellipse(i, y, dia, dia);

  //image(img, 0,0);
}

function draw() {
  //background(220);
  reset_button.mouseClicked(reset_wheel);
  move_button.mouseClicked(button_status);
  if(move_buttonStatus == true)
  {
    move_wheel();
  }
  stroke(100);
  strokeWeight(1);
  line(100, y+51, 550, y+51);

  let x = color(2, 204, 0);
  stroke(x);
  strokeWeight(5);
  line(500,254,500,248);

}
